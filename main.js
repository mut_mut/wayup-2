//1

for (let i = 1; i <= 50; i++) {
    if (i % 2 == 0) {
        console.log(i);
    }
}

//2

const iam = {
    name: 'Fedya',
    lastname: 'Pupkin',
    age: 15,
    animal: 'cat'
}

//3

// "В ту же ночь приехал я в Симбирск, где должен был пробыть сутки для закупки нужных вещей,
// что и было поручено Савельичу. Я остановился в трактире. Савельич с утра отправился по лавкам"

//Массив: 
const array = [
    'я в Симбирск,',//1 - 2
    'в трактире.',//2 - 9
    'с утра',//3 - 11
    'В ту же ночь',//4 - 1 
    'Я остановился',//5 - 8
    'для закупки', //6 - 5
    'что и было поручено Савельичу.',//7 -7
    'приехал',//8 - 1+
    'где должен был',//9 - 3
    'нужных вещей',//10 - 6
    'отправился по лавкам',//11 - 12
    'пробыть сутки',//12 - 4
    'Савельич'//13 - 10
]

let result = "";

const pushkin = (i) => {
    return array[i];
}

const newArr = [4, 8, 1, 9, 12, 6, 10, 7, 5, 2, 13, 3, 11];

newArr.forEach(el => {
    result = result + " " + pushkin(el - 1);
});

console.log(result);

//4

const fullName = (firstName, lastName) => {
    const fullName = `${firstName} ${lastName}`;
    console.log(fullName);
}

fullName('Fedya', 'Pupkin');

//5

let i = 21;
let n = 67;

while (i <= n) {
    if (i % 2 != 0) {
        console.log(i);
    }
    i++;
}